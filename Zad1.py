def dec(func):
    def inner(*args, **kwargs):
        inner.sch += 1
        return func(*args, **kwargs)

    inner.sch = 0
    return inner


@dec
def phonk():
    return 1


phonk()
phonk()
print('Количество вызываемых функций =', phonk.sch)
