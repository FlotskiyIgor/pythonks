def sum_range(a, z):
    summ = 1
    for i in range(a, z + 1):
        summ *= i
    return summ


print(sum_range(1, 7))
