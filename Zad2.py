from time import time
def dec(func):
    def method(*args, **kwargs):
        first_time = time()
        result = func(*args, **kwargs)
        second_time = time()
        result_time = second_time-first_time
        print(f'Время выполнения функции составляет {(result_time):.5f} секунд')
        return result
    return method

@dec
def time_dec(n):
   for i in range(n):
        for j in range(n**n):
            ((i*j)/2)**2


time_dec(6)